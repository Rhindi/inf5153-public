package patterns.comportement.commande

class Telecommande {

    def boutons = [:]

    def charger(String bouton, Commande commande) {
        boutons[bouton] = commande
    }

    def clic(String bouton) {
        println "\nBouton cliqué : $bouton"
        boutons[bouton]?.executer()
    }

}
