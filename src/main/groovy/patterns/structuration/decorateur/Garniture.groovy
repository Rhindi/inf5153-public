package patterns.structuration.decorateur

abstract class Garniture implements Cafe {

    final Cafe cafe

    Garniture(Cafe cafe, String description, BigDecimal prix) {
        this.cafe = cafe
        this.description = "$cafe.description avec $description"
        this.prix = cafe.prix + prix
    }

}
