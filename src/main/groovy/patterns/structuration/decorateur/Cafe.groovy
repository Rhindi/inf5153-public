package patterns.structuration.decorateur

trait Cafe {

    String description

    BigDecimal prix

    String getPrixAsDollar() {
        java.text.NumberFormat.currencyInstance.format prix
    }

    String toString() {
        "$description à $prixAsDollar"
    }

}
