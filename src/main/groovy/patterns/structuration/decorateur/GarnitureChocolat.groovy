package patterns.structuration.decorateur

class GarnitureChocolat extends Garniture {

    GarnitureChocolat(Cafe cafe) {
        super(cafe, "chocolat", 0.20)
    }

}
