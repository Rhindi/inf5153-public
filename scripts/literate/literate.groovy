//
// literate
//
// Utilise le contenu du fichier intrant
// pour créer un fichier markdown et
// pour créer un fichier pdf
//

principal()

def principal() {
    String fichierIntrant = args[0]
    println "Fichier intrant : $fichierIntrant"
    def lignes = new File(fichierIntrant).readLines()
    def markdown = genererMarkdown(lignes)
    genererPdf(fichierIntrant, markdown)
}

def genererMarkdown(lignes) {
    def buffer = StringBuilder.newInstance()
    boolean modeCode = false
    lignes.each { ligne ->
        switch(ligne) {
            case ~'':
            case ~'^//$':
                break
            case ~'^//.*': // Texte
                if ( modeCode ) {
                    terminerCode(buffer)
                    modeCode = false
                }
                buffer << ligne.substring(3)
                break
            default: // Code
                if ( !modeCode ) {
                    modeCode = true
                    buffer << '```java\n'
                }
                buffer << ligne
        }
        buffer << '\n'
    }
    if ( modeCode ) {
        terminerCode(buffer)
    }
    buffer
}

def terminerCode(buffer) {
    def dernier = buffer.length() - 1
    if ( buffer.substring(dernier - 1) == '\n\n' ) {
        buffer.deleteCharAt(dernier)
    }
    buffer << '```\n\n'
}

def genererPdf(fichierIntrant, markdown) {
    def fichierPrefixe = fichierIntrant.take(fichierIntrant.lastIndexOf('.'))
    def fichierMarkdown = fichierPrefixe + '.md'
    def fichierPdf = fichierPrefixe + '.pdf'
    new File(fichierMarkdown).text = markdown
    String commande = "pandoc $fichierMarkdown -o $fichierPdf -N --toc --variable=lang:fr --variable=documentclass:scrartcl"
    commande.execute().waitForProcessOutput(System.out, System.err)
}
