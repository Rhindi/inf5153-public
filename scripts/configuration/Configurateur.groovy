class Configurateur {

// http://mrhaki.blogspot.ca/2014/05/groovy-goodness-extend-configslurper.html
// http://mrhaki.blogspot.ca/2015/09/gradle-goodness-pass-java-system.html

    static ConfigObject chargerConfig(URL configLocation, String mode = '') {
        def environnement = mode?:(System.getProperty('environnement')?:'test')
        def slurper = new ConfigSlurper(environnement)
        slurper.registerConditionalBlock('variations', environnement)
        slurper.parse(configLocation)
    }

    static ConfigObject chargerConfig(
            String nomFichier = 'configuration.groovy',
            String mode = '') {
        URL url = java.nio.file.Paths.get(nomFichier).toUri().toURL()
        chargerConfig(url, mode)
    }

}
