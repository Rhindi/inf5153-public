// Au préalable, le serveur de base de données PostgreSQL doit être lancé
@Grapes( [
    @Grab('org.postgresql:postgresql:9.4.1211.jre7'),
    @Grab('commons-dbcp:commons-dbcp:1.4'),
    @GrabConfig(systemClassLoader=true),
] )

import static Persistance.avecSql
import static Persistance.executerSql
import static Persistance.executerInsertSql

def generateur = ContactGenerateur.instance

// executerSql '''
//     DROP TABLE IF EXISTS contacts;
//     CREATE TABLE contacts (
//         id SERIAL PRIMARY KEY,
//         data JSONB
//     );
// '''

// def resultat = executerInsertSql '''
//     INSERT INTO contacts (data) VALUES (CAST(? as JSON));
// ''', [generateur.genererContact().data]
//
// println resultat

// Pour insérer du JSON voir https://github.com/pgjdbc/pgjdbc/issues/265

// def insertion = 'INSERT INTO contacts (data) VALUES (CAST(? as JSON));'
// avecSql { sql ->
//     10000.times {
//         def contact = [ generateur.genererContact().data ]
//         sql.executeInsert insertion, contact
//     }
// }

// http://schinckel.net/2014/05/25/querying-json-in-postgres/
// http://stormatics.com/howto-use-json-functionality-in-postgresql/
// select count(*) from contacts where data ? 'personne';
// select count(*) from contacts where data ? 'organisation';
// SELECT count(*) FROM contacts WHERE data #>> '{personne,prenom}' = 'Maurice';

// SELECT data -> 'personne' ->> 'prenom' as prenom,
//        data -> 'personne' ->> 'nomFamille' as famille
//        FROM contacts WHERE data #>> '{personne,prenom}' = 'Maurice';
