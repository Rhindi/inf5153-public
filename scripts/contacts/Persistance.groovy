import groovy.sql.Sql
import org.apache.commons.dbcp.BasicDataSource

class Persistance {

    static final dataSource = new BasicDataSource(
        url: 'jdbc:postgresql://localhost/louis',
        username: 'louis',
        password: '',
        driverClassName: 'org.postgresql.Driver'
    )

    static avecSql(closure) {
        def sql = new Sql(dataSource)
        try {
            closure( sql )
        } finally {
            sql.close()
        }
    }

    static executerSql(enonce) {
        avecSql { sql -> sql.execute enonce }
    }

    static executerInsertSql(enonce, valeurs) {
        avecSql { sql -> sql.executeInsert enonce, valeurs }
    }

}
