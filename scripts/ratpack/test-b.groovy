@Grapes( [
    @Grab('io.ratpack:ratpack-groovy:1.4.2'),
    @Grab('org.slf4j:slf4j-simple:1.7.21'),
] )

import static ratpack.groovy.Groovy.ratpack
import static groovy.json.JsonOutput.toJson

class Utilisateur {
    String nom
    String courriel
}

def utilisateurs = [
    new Utilisateur(nom: 'louis', courriel: 'louis@gmail.com'),
    new Utilisateur(nom: 'marie', courriel: 'marie@gmail.com'),
    new Utilisateur(nom: 'jean', courriel: 'jean@gmail.com'),
]

ratpack {
    handlers {
        get("utilisateurs") {
            byContent {
                html {
                    def utilisateursHtml = utilisateurs.collect { utilisateur ->
                        """\
                        |<div>
                        |<b>Utilisateur:</b> ${utilisateur.nom}
                        |<b>Courriel:</b> ${utilisateur.courriel}
                        |</div>
                        """.stripMargin()
                    }.join()
                    render """\
                        |<!DOCTYPE html>
                        |<html>
                        |<head>
                        |<title>Liste des utilisateurs</title>
                        |</head>
                        |<body>
                        |<h1>Utilisateurs</h1>
                        |${utilisateursHtml}
                        |</body>
                        |</html>
                        """.stripMargin()
                }
                json {
                    render toJson(utilisateurs)
                }
                xml {
                    def utilisateursXml = utilisateurs.collect { utilisateur ->
                        """\
                        <utilisateur>
                            <nom>${utilisateur.nom}</nom>
                            <courriel>${utilisateur.courriel}</courriel>
                        </utilisateur>
                        """.stripIndent(20)
                    }.join()
                    render "<utilisateurs>\n${utilisateursXml}</utilisateurs>\n"
                }
            }
        }
    }
}
