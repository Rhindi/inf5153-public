@Grapes( [
    @Grab('io.ratpack:ratpack-groovy:1.4.2'),
    @Grab('org.slf4j:slf4j-simple:1.7.21'),
] )

import static ratpack.groovy.Groovy.ratpack

ratpack {
    handlers {
        get {
            render 'Bonjour le monde !\n'
        }
        prefix('produits') {
            get('lister') {
                render 'Voici la liste...\n'
            }
            get('obtenir') {
                render 'Voici le produit...\n'
            }
            get('chercher') {
                render 'Voici la recherche...\n'
            }
        }
        path('salut') {
            byMethod {
                get {
                    render 'Salut pour un GET\n'
                }
                post {
                    render 'Salut pour un POST\n'
                }
            }
        }
        get('pdf') {
            render file('./test.pdf')
        }
    }
}
