@Grapes( [
    @Grab('ch.qos.logback:logback-classic:1.1.7'),
    @GrabConfig(systemClassLoader=true)
] )

import groovy.util.logging.Slf4j

@Slf4j
class Master {

    static void main(String[] args) {
        log.info 'Début de l\'exécution'
        def bidule = new Bidule()
        bidule.saluer()
        log.info 'Fin de l\'exécution'
    }

}

// Voir http://www.slf4j.org/
// Voir http://logback.qos.ch/
// Voir https://www.loggly.com/blog/benchmarking-java-logging-frameworks/
//
// Annotations possibles avec Groovy selon http://mrhaki.blogspot.ca/2011/04/groovy-goodness-inject-logging-using.html
//
// Logging framework        Annotation
// java.util.logging        @Log
// Log4j                    @Log4j
// Apache Commons Logging   @Commons
// Slf4j API                @Slf4j
