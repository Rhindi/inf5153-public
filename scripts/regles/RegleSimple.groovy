import org.easyrules.annotation.*

@Rule(name='Règle simple', description='Règle définie par annotation')
class RegleSimple {

    @Condition
    boolean toujoursVrai() {
        true
    }

    @Action(order=1)
    def caFonctionne() {
        println 'Easy Rules fonctionne A !'
    }

    @Action(order=2)
    def pourFinir() {
        println 'Fin de la règle'
    }

}
