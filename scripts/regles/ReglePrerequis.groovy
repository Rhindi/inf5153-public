import org.easyrules.annotation.*

@Rule(name='RèglePrerequis', description='Vérification des prérequis')
class ReglePrerequis {

    static prerequis = [
        INF2120: [ 'INF1120' ],
        INF2170: [ 'INF1120' ],
        INF3172: [ 'INF2170', 'INF3135' ],
    ]

    def dossierEtudiant = null

    @Condition
    boolean verifierPrerequis() {
        prerequisComplet(dossierEtudiant, prerequis)
    }

    @Action
    def permettreInscription() {
        dossierEtudiant.messages << 'Prérequis satisfaits'
    }

    boolean prerequisComplet(dossier, prerequis) {
        def resultat = true
        dossier.coursDemandes.each { cours ->
            def liste = prerequis[cours] ?: []
            liste.each { prealable ->
                if ( ! ( prealable in dossier.coursReussis ) ) {
                    dossier.messages <<
                        "Préalable $prealable non respecté pour $cours"
                    resultat = false
                }
            }
        }
        resultat
    }

}
